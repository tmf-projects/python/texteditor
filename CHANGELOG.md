# TextEditor Changelog

## RUS

### Версия 20201130_1

- Новое:
	- Настройки:
		- Перенос строки
		- Сохранение настроек
	- Горячие клавиши:
		- Ctrl+N - "Новый"
		- Ctrl+O - "Открыть"
		- Ctrl+S - "Сохранить"
		- Ctrl+Shift+S - "Сохранить как..."
		- Ctrl+E - "Выход"
		- Ctrl+I - "О программе"
		- Alt+S - "Настройки"
- Скроллбары теперь скрываются, когда в них нет необходимости
- Исправил логику выхода из программы
- Заменил окно диалога, теперь с кнопкой "Отмена"
- Экспериментирую с основным шрифтом...
- Увеличил окно до 1280х720
- Гланое окно, окно настроек, и окно "О программе" стараются открываться в середине экрана
- Нумерация строк теперь имеет тот же шрифт, что и у текста

### Версия 20201127_1

- Новое:
	- Нумерация строк
	- Статус редактирования файла (если после названия файла присутствует символ "\*" - значит вы редактировали файл)
- Исправил "прикол" - После нажатии "Создать" при редактировании безымянного файла, и успешного сохранения нового файла, программа не создавала новый безымянный файл
- Мелочи:
	- Убрал дублирующийся код, но не везде
	- Убрал ненужные комментарии

### Версия 20201125_1

Программа переписана полностью, чтобы внедрить классы!

- Новое:
	- Меню сверху:
		- "Файл":
			- "Создать"
			- "Открыть"
			- "Сохранить"
			- "Сохранить как..."
			- "Выход"
		- "Справка":
			- "О программе"
- Теперь исправно работают скроллы и многостроковый виджет
- Кажется, что где-то есть "прикол" с логикой программы (забыл, где)

### Версия без номера

Первый релиз!

## ENG

### Version 20201130_1

- New:
	- Settings:
		- Transferring a line
		- Saving settings
	- Hot keys:
		- Ctrl+N - "New"
		- Ctrl+O - "Open"
		- Ctrl+S - "Save"
		- Ctrl+Shift+S - "Save as...".
		- Ctrl+E - "Exit"
		- Ctrl+I - "About the program"
		- Alt+S - "Settings"
- Scrollbars are now hidden when they are not necessary
- Fixed program exit logic
- Replaced the dialog box, now with the "Cancel" button
- I am experimenting with the main font...
- Increased the window to 1280x720
- The main window, the settings window, and the "About" window try to open in the middle of the screen
- Line numbering now has the same font as text

### Version 20201127_1

- New:
	- Numbering of lines
	- File editing status (if the symbol "\*" is present after the file name, it means you have edited the file)
- Fixed "joke" - After clicking "Create" when editing an untitled file, and successfully saving a new file, the program did not create a new untitled file
- Little things:
	- Removed the duplicated code, but not everywhere
	- Removed unnecessary comments

### Version 20201125_1

The program is completely rewritten to implement the classes!

- New:
	- Menu at the top:
		- "File":
			- "Create"
			- "Open"
			- "Save"
			- "Save as..."
			- "Exit"
		- "Help":
			- "About the program"
- Now working properly scrolls and multi-line widget
- It seems that somewhere there is a "joke" with the logic of the program (I forgot where)

### Version without number

First release!