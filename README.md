# TextEditor

### Простой текстовый редактор

## Скриншоты

![Безымянный файл](screenshots/20201127_1/untitled.png) ![Открытый файл](screenshots/20201127_1/open_text.png)

## Сборка проекта

### В одну папку

`pyinstaller --noconsole --name TextEditor main.py`

### В один файл (запускается медленнее)

`pyinstaller --noconsole --onefile --name TextEditor main.py`

## Скриншоты версии 20201125_1

![Безымянный файл](screenshots/20201125_1/untitled.png) ![Открытый файл](screenshots/20201125_1/open_text.png)

## Скриншоты первой версии

![Безымянный файл](screenshots/old/untitled.png) ![Открытый файл](screenshots/old/open_text.png)
