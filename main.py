# чтобы взять имя файла без расширения,
# нужно написать
# os.path.splitext(os.path.basename(f.name))[0]
# https://ru.stackoverflow.com/questions/1139528/%D0%9A%D0%B0%D0%BA-%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D1%82%D1%8C-%D0%BD%D1%83%D0%BC%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D1%8E-%D1%81%D1%82%D1%80%D0%BE%D0%BA-%D0%B4%D0%BB%D1%8F-%D1%82%D0%B5%D0%BA%D1%81%D1%82%D0%BE%D0%B2%D0%BE%D0%B3%D0%BE-%D0%BF%D0%BE%D0%BB%D1%8F-%D0%B2-tkinter

from tkinter import *
from tkinter import filedialog as fd
from tkinter import messagebox as mbox
import os
import json

version = "20201130_1"
creator = "TheMysticalFox (Даниил Басалаев)\n2020 год"

width = 1280
height = 720

font_text = ("Consolas", 14)

file_name = "Безымянный"
file_path = ""
file_text = ['']


class App:
    def __init__(self, window):
        self.main_frame = Frame()
        self.text_frame = Frame(self.main_frame)
        self.X_frame = Frame(self.main_frame)
        self.Y_frame = Frame(self.main_frame)

        self.main_menu = Menu(window)
        self.file_menu = Menu(self.main_menu, tearoff=0)
        self.info_menu = Menu(self.main_menu, tearoff=0)

        self.line_number = TextLineNumbers(self.text_frame, width=30)
        self.text = MultilineText(self.text_frame, wrap=NONE, font=font_text,
                                  width=0, height=0)
        self.line_number.attach(self.text)
        self.text.bind("<<Change>>", self._on_change)
        self.text.bind("<Configure>", self._on_change)

        self.scroll_Y = Scrollbar(self.Y_frame, command=self.text.yview)
        self.scroll_X = Scrollbar(self.X_frame, orient=HORIZONTAL, command=self.text.xview)

        self.init(window)

    def init(self, window):
        self.init_structure(window)

    def init_structure(self, window):
        self.init_menu(window)

        self.main_frame.pack(expand=True, fill=BOTH)
        self.Y_frame.pack(side=RIGHT, fill=Y)
        self.text_frame.pack(expand=True, fill=BOTH)
        self.X_frame.pack(side=BOTTOM, fill=X)

        self.init_widgets()

    def init_widgets(self):
        self.line_number.pack(fill=Y, side=LEFT)
        self.text.pack(expand=True, fill=BOTH)

        self.scroll_Y.pack(side=RIGHT, fill=Y)
        self.text.config(yscrollcommand=self.scroll_Y.set)

        self.scroll_X.pack(side=BOTTOM, fill=X)
        self.text.config(xscrollcommand=self.scroll_X.set)

    def init_menu(self, window):
        window.config(menu=self.main_menu)

        self.file_menu.add_command(label="Создать", command=lambda: self.new_file(),
                                   accelerator="Ctrl+N")
        self.file_menu.add_command(label="Открыть", command=lambda: self.open_file(),
                                   accelerator="Ctrl+O")
        self.file_menu.add_command(label="Сохранить", command=lambda: Save.save(self.text),
                                   accelerator="Ctrl+S")
        self.file_menu.add_command(label="Сохранить как...", command=lambda: self.save_as_file(),
                                   accelerator="Ctrl+Shift+S")
        self.file_menu.add_command(label="Выход", command=lambda: self.exit_program(window),
                                   accelerator="Ctrl+E")

        self.info_menu.add_command(label="О программе", command=lambda: self.about_program(window),
                                   accelerator="Ctrl+I")

        self.main_menu.add_cascade(label="Файл", menu=self.file_menu)
        self.main_menu.add_command(label="Настройки", command=lambda: self.program_settings(window),
                                   accelerator="Alt+S")
        self.main_menu.add_cascade(label="Справка", menu=self.info_menu)

    def _on_change(self, event):
        self.line_number.redraw()
        self.checking()

    def checking(self):
        self.check_text(root)
        self.check_scroll()
        self.text_wrap()

    def check_text(self, window):
        global file_name, file_path, file_text

        if file_text[0] != self.text.get(1.0, 'end-1c'):
            if file_path != "":
                window.title(file_name + "* - " + file_path)
            else:
                window.title(file_name + "*")
        elif file_text[0] == self.text.get(1.0, 'end-1c'):
            if file_path != "":
                window.title(file_name + " - " + file_path)
            else:
                window.title(file_name)

    def text_wrap(self):
        if text_wrap.get() == 1:
            self.text.config(wrap=WORD)
        elif text_wrap.get() == 0:
            self.text.config(wrap=NONE)

    def check_scroll(self):
        if self.scroll_X.get() < (0.0, 1.0) or self.scroll_X.get() > (0.0, 1.0):
            self.X_frame.pack_forget()
            self.X_frame.pack(side=BOTTOM, fill=X)
        else:
            self.X_frame.pack_forget()

        if self.scroll_Y.get() < (0.0, 1.0) or self.scroll_Y.get() > (0.0, 1.0):
            self.text_frame.pack_forget()
            self.Y_frame.pack_forget()
            self.Y_frame.pack(side=RIGHT, fill=Y)
            self.text_frame.pack(expand=True, fill=BOTH)
        else:
            self.Y_frame.pack_forget()

    def question_new(self):
        global file_name, file_path, file_text

        result = Save.ask_save_cancel()
        if result is True:
            save_result = self.save_as_file()
            if save_result:
                file_name = "Безымянный"
                file_path = ""
                file_text = ['']
                self.text.delete(1.0, END)
        elif result is False:
            file_name = "Безымянный"
            file_path = ""
            file_text = ['']
            self.text.delete(1.0, END)
        elif result is None:
            print("Произошла отмена сценария создания нового файла")

    def new_file(self):
        if file_path != "":
            self.question_new()
        elif self.text.get(1.0, 'end-1c') != "" and file_path == "":
            self.question_new()

    def open_file(self):
        global file_name, file_path, file_text

        file = fd.askopenfilename()
        if file != "":
            with open(file, encoding="utf-8") as f:
                t = f.read()
                file_path = f.name
                file_name = os.path.basename(file_path)
                self.text.delete(1.0, END)
                self.text.insert(1.0, t)

                if len(file_text) > 0:
                    file_text.pop()
                    file_text.append(t)
        root.after(100, app.checking)

    def save_as_file(self):
        result = Save.save_file_name_path(self.text)
        return result

    def exit_program(self, window):
        if self.text.get(1.0, 'end-1c') != "" and file_path == "":
            question = Save.ask_save_cancel()
            if question:
                self.save_as_file()
            elif question is False:
                window.destroy()
            elif question is None:
                print("Отмена сценария выхода из программы")
        elif self.text.get(1.0, 'end-1c') == "" and file_path == "":
            window.destroy()
        elif self.text.get(1.0, 'end-1c') != "" and file_path != "":
            if file_text[0] == self.text.get(1.0, 'end-1c'):
                window.destroy()
            else:
                question = Save.ask_save_cancel()
                if question:
                    Save.save(self.text)
                    window.destroy()
                elif question is False:
                    window.destroy()
                elif question is None:
                    print("Отмена сценария выхода из программы")

    @staticmethod
    def about_program(window):
        About(window)

    @staticmethod
    def program_settings(window):
        Settings(window)


class Save:
    @staticmethod
    def save(text: Text):
        global file_name, file_path, file_text

        if file_path != "":
            with open(file_path, 'w', encoding="utf-8") as file:
                t = text.get(1.0, 'end-1c')
                file.write(t)

                if len(file_text) > 0:
                    file_text.pop()
                    file_text.append(t)
            print("Сохранение завершено успешно!")
            app.check_text(root)
        else:
            Save.save_file_name_path(text)

    @staticmethod
    def ask_save():
        result = mbox.askyesno("Вопрос", "Вы хотите сохранить файл?")
        return result

    @staticmethod
    def ask_save_cancel():
        result = mbox.askyesnocancel("Вопрос", "Вы хотите сохранить файл?")
        return result

    @staticmethod
    def save_file_name_path(text: Text):
        global file_name, file_path, file_text

        file = fd.asksaveasfilename(
            filetypes=(("All files", "*.*"),
                       ("TXT files", "*.txt"),
                       ("HTML files", "*.html;*.htm"),
                       ("PYTHON files", "*.py")),
            initialfile=file_name)
        if file != "":
            with open(file, 'w', encoding="utf-8") as f:
                t = text.get(1.0, 'end-1c')
                f.write(t)
                file_path = f.name
                file_name = os.path.basename(file_path)
                print(file_name)
                print(file_path)

                if len(file_text) > 0:
                    file_text.pop()
                    file_text.append(t)
            print("Сохранение завершено успешно!")
            app.check_text(root)
            return True
        else:
            print("Сохранение отклонено или завершилось с ошибкой "
                  "\"Путь не найден!\"")
            file_name = "Безымянный"
            return False


class About(Toplevel):
    def __init__(self, window):
        super().__init__(window)
        self.title("О программе")
        self.geometry("400x100+600+300")
        self.resizable(0, 0)

        self.program_name = Label(self, text="TextEditor", font=font_text)
        self.program_version = Label(self, text="Версия: " + version, font=font_text)
        self.program_creator = Label(self, text=creator, font=font_text)
        self.init()

    def init(self):
        self.init_widgets()

    def init_widgets(self):
        self.program_name.pack()
        self.program_version.pack()
        self.program_creator.pack()


class Settings(Toplevel):
    def __init__(self, window):
        super().__init__(window)
        self.title("Настройки")
        self.minsize(400, 200)
        self.geometry("400x200+600+300")
        self.maxsize(800, 400)
        self.resizable(0, 0)

        self.text_wrap_cb = Checkbutton(self, text="Перенос строк", font=font_text,
                                        variable=text_wrap, offvalue=0, onvalue=1, command=lambda: conf.update_json())

        self.init()

    def init(self):
        self.init_widgets()

    def init_widgets(self):
        self.text_wrap_cb.pack()


class MultilineText(Text):
    def __init__(self, *args, **kwargs):
        Text.__init__(self, *args, **kwargs)

        # create a proxy for the underlying widget
        self._orig = self._w + "_orig"
        self.tk.call("rename", self._w, self._orig)
        self.tk.createcommand(self._w, self._proxy)

    def _proxy(self, *args):
        # let the actual widget perform the requested action
        cmd = (self._orig,) + args
        try:
            result = self.tk.call(cmd)
        except TclError:
            print("Что-то произошло с программой, сработала защита от падения")
            return None

        # generate an event if something was added or deleted,
        # or the cursor position changed
        if (args[0] in ("insert", "replace", "delete") or
                args[0:3] == ("mark", "set", "insert") or
                args[0:2] == ("xview", "moveto") or
                args[0:2] == ("xview", "scroll") or
                args[0:2] == ("yview", "moveto") or
                args[0:2] == ("yview", "scroll")):
            self.event_generate("<<Change>>", when="tail")

        # return what the actual widget returned
        return result


class TextLineNumbers(Canvas):
    def __init__(self, *args, **kwargs):
        Canvas.__init__(self, *args, **kwargs)
        self.text_widget = None

    def attach(self, text_widget):
        self.text_widget = text_widget

    def redraw(self, *args):
        """redraw line numbers"""
        self.delete("all")

        i = self.text_widget.index("@0,0")
        while True:
            d_line = self.text_widget.dlineinfo(i)
            if d_line is None:
                break
            y = d_line[1]
            line_num = str(i).split(".")[0]
            self.create_text(2, y, anchor="nw", text=line_num, font=font_text)
            i = self.text_widget.index("%s+1line" % i)


class JSONConfig:
    def __init__(self):
        self.json_path = os.getcwd() + r"\settings.json"

        self.text_wrap = 0

        self._json_new = {'text_wrap': self.text_wrap}
        self.config = ""
        self.check_json()

    def create_json(self):
        with open(self.json_path, 'w') as f:
            json.dump(self._json_new, f)

    def open_json(self):
        with open(self.json_path, 'r') as f:
            self.config = json.load(f)

        if self.config['text_wrap'] == 0:
            text_wrap.set(0)
        else:
            text_wrap.set(1)

    def check_json(self):
        if not os.path.isfile(self.json_path):
            self.create_json()
        else:
            self.open_json()

    def update_json(self):
        if not text_wrap.get():
            self.text_wrap = 0
        else:
            self.text_wrap = 1

        root.after(500, app.text_wrap)

        self._json_new = {'text_wrap': self.text_wrap}
        self.create_json()
        print("Сохранение настроек завершился успешно!")


def control_key(event):
    if event.keycode == 78:
        print("Выполнена команда \"Новый\"")
        app.new_file()
    elif event.keycode == 79:
        print("Выполнена команда \"Открыть\"")
        app.open_file()
    elif event.keycode == 83:
        print("Выполнена команда \"Сохранить\"")
        Save.save(app.text)
    elif event.keycode == 69:
        print("Выполнена команда \"Выход\"")
        app.exit_program(root)
    elif event.keycode == 73:
        print("Выполнена команда \"О программе\"")
        app.about_program(root)


def control_shift_key(event):
    if event.keycode == 83:
        print("Выполнена команда \"Сохранить как...\"")
        app.save_as_file()


def alt_key(event):
    if event.keycode == 83:
        print("Выполнена команда \"Настройки\"")
        app.program_settings(root)


root = Tk()

text_wrap = BooleanVar()
text_wrap.set(0)

app = App(root)
conf = JSONConfig()

root.geometry(str(width) + "x" + str(height) + "+" + str(int(width / 8)) + "+" + str(int(height / 16)))

root.after(1000, app.checking)

root.bind('<Control-KeyRelease>', control_key)
root.bind('<Control-Shift-KeyRelease>', control_shift_key)
root.bind('<Alt-KeyRelease>', alt_key)

root.mainloop()
